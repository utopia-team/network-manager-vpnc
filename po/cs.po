# Czech translation of NetworkManager-vpnc.
# Copyright (C) 2008, 2009, 2010, 2011 the author(s) of NetworkManager-vpnc.
# This file is distributed under the same license as the NetworkManager-vpnc package.
#
# Jakub Friedl <jfriedl@suse.cz>, 2006.
# Jiří Eischmann <jiri@eischmann.cz>, 2008.
# Zdeněk Hataš <zdenek.hatas@gmail.com>, 2009 - 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: NetworkManager-vpnc\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/NetworkManager-vpnc/\n"
"POT-Creation-Date: 2024-11-15 13:33+0100\n"
"PO-Revision-Date: 2021-03-25 16:15+0100\n"
"Last-Translator: Zdeněk Hataš <zdenek.hatas@gmail.com>\n"
"Language-Team: Czech <gnome-cs-list@gnome.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Poedit 2.3\n"

#: appdata/network-manager-vpnc.metainfo.xml.in:10
msgid "Legacy Cisco VPNC client"
msgstr "Původní klient Cisco VPNC"

#: appdata/network-manager-vpnc.metainfo.xml.in:11
msgid "Client for Cisco IPsec virtual private networks"
msgstr "Klient pro virtuální privátní sítě Cisco IPSec"

#: appdata/network-manager-vpnc.metainfo.xml.in:14
msgid "network"
msgstr ""

#: appdata/network-manager-vpnc.metainfo.xml.in:15
msgid "manager"
msgstr ""

#: appdata/network-manager-vpnc.metainfo.xml.in:16
#, fuzzy
#| msgid "The NetworkManager Developers"
msgid "NetworkManager"
msgstr "Vývojáři aplikace NetworkManager"

#: appdata/network-manager-vpnc.metainfo.xml.in:17
msgid "connection"
msgstr ""

#: appdata/network-manager-vpnc.metainfo.xml.in:18
msgid "VPN"
msgstr ""

#: appdata/network-manager-vpnc.metainfo.xml.in:19
msgid "VPNC"
msgstr ""

#: appdata/network-manager-vpnc.metainfo.xml.in:20
msgid "IPsec"
msgstr ""

#: appdata/network-manager-vpnc.metainfo.xml.in:21
#, fuzzy
#| msgid "Cisco UDP"
msgid "Cisco"
msgstr "Cisco UDP"

#: appdata/network-manager-vpnc.metainfo.xml.in:25
msgid "Support for configuring virtual private networks based on VPNC."
msgstr "Podpora nastavení sítí VPN založených na VPNC."

#: appdata/network-manager-vpnc.metainfo.xml.in:26
msgid "Compatible with Cisco VPN concentrators configured to use IPsec."
msgstr "Kompatibilní s koncentrátory Cisco VPN nastavenými pro použití IPsec."

#: appdata/network-manager-vpnc.metainfo.xml.in:34
#, fuzzy
#| msgid "Advanced Options"
msgid "The advanced options dialog"
msgstr "Pokročilé volby"

#: appdata/network-manager-vpnc.metainfo.xml.in:43
msgid "The NetworkManager Developers"
msgstr "Vývojáři aplikace NetworkManager"

#: auth-dialog/main.c:164 auth-dialog/main.c:223
msgid "Authenticate VPN"
msgstr "Ověřování VPN"

#: auth-dialog/main.c:178
msgid "Password"
msgstr "Heslo"

#: auth-dialog/main.c:186
msgid "Group Password"
msgstr "Heslo skupiny"

#: auth-dialog/main.c:232
msgid "_Group Password:"
msgstr "Heslo _skupiny:"

#: auth-dialog/main.c:489
#, c-format
msgid "You need to authenticate to access the Virtual Private Network “%s”."
msgstr "Pro přístup do VPN „%s“ se musí provést ověření."

#: properties/nm-vpnc-editor-plugin.c:40
msgid "Cisco Compatible VPN (vpnc)"
msgstr "VPN kompatibilní s Cisco (vpnc)"

#: properties/nm-vpnc-editor-plugin.c:41
msgid ""
"Compatible with various Cisco, Juniper, Netscreen, and Sonicwall IPsec-based "
"VPN gateways."
msgstr ""
"Kompatibilní s různými bránami VPN založenými na IPsec od Cisco, Juniper, "
"Netscreen a Sonicwall."

#: properties/nm-vpnc-editor-plugin.c:488
#, c-format
msgid ""
"The VPN settings file “%s” specifies that VPN traffic should be tunneled "
"through TCP which is currently not supported in the vpnc software.\n"
"\n"
"The connection can still be created, with TCP tunneling disabled, however it "
"may not work as expected."
msgstr ""
"Soubor nastavení VPN „%s“ udává, že má být spojení VPN tunelováno přes TCP. "
"To není v současné době ve vpnc podporováno.\n"
"\n"
"Připojení půjde navázat bez TCP tunelování, ale je možné, že nebude fungovat "
"korektně."

#: properties/nm-vpnc-editor.c:685
msgid "Secure (default)"
msgstr "Zabezpečené (výchozí)"

#: properties/nm-vpnc-editor.c:688
msgid "Weak (use with caution)"
msgstr "Slabé (používejte obezřetně)"

#: properties/nm-vpnc-editor.c:691
msgid "None (completely insecure)"
msgstr "Žádné (kompletně nezabezpečené)"

#: properties/nm-vpnc-editor.c:735
msgid "Cisco (default)"
msgstr "Cisco (výchozí)"

#: properties/nm-vpnc-editor.c:738
msgid "Netscreen"
msgstr "Netscreen"

#: properties/nm-vpnc-editor.c:741
msgid "Fortigate"
msgstr ""

#: properties/nm-vpnc-editor.c:752
msgid "NAT-T when available (default)"
msgstr "NAT-T, pokud je dostupné (výchozí)"

#: properties/nm-vpnc-editor.c:755
msgid "NAT-T always"
msgstr "Vždy NAT-T"

#: properties/nm-vpnc-editor.c:758
msgid "Cisco UDP"
msgstr "Cisco UDP"

#: properties/nm-vpnc-editor.c:761
msgid "Disabled"
msgstr "Zakázáno"

#: properties/nm-vpnc-editor.c:772 properties/nm-vpnc-editor.c:810
msgid "DH Group 1"
msgstr "Skupina DH 1"

#: properties/nm-vpnc-editor.c:775
msgid "DH Group 2 (default)"
msgstr "Skupina DH 2 (výchozí)"

#: properties/nm-vpnc-editor.c:778 properties/nm-vpnc-editor.c:816
msgid "DH Group 5"
msgstr "Skupina DH 5"

#: properties/nm-vpnc-editor.c:781 properties/nm-vpnc-editor.c:819
#, fuzzy
#| msgid "DH Group 1"
msgid "DH Group 14"
msgstr "Skupina DH 1"

#: properties/nm-vpnc-editor.c:784 properties/nm-vpnc-editor.c:822
#, fuzzy
#| msgid "DH Group 1"
msgid "DH Group 15"
msgstr "Skupina DH 1"

#: properties/nm-vpnc-editor.c:787 properties/nm-vpnc-editor.c:825
#, fuzzy
#| msgid "DH Group 1"
msgid "DH Group 16"
msgstr "Skupina DH 1"

#: properties/nm-vpnc-editor.c:790 properties/nm-vpnc-editor.c:828
#, fuzzy
#| msgid "DH Group 1"
msgid "DH Group 17"
msgstr "Skupina DH 1"

#: properties/nm-vpnc-editor.c:793 properties/nm-vpnc-editor.c:831
#, fuzzy
#| msgid "DH Group 1"
msgid "DH Group 18"
msgstr "Skupina DH 1"

#: properties/nm-vpnc-editor.c:804
msgid "Server (default)"
msgstr "Server (výchozí)"

#: properties/nm-vpnc-editor.c:807
msgid "None"
msgstr "Žádný"

#: properties/nm-vpnc-editor.c:813
msgid "DH Group 2"
msgstr "Skupina DH 2"

#: src/nm-vpnc-service.c:204
#, c-format
msgid "property “%s” invalid or not supported"
msgstr "volba „%s“ není platná nebo podporovaná"

#: src/nm-vpnc-service.c:219
#, c-format
msgid "property “%s” contains a newline character"
msgstr "volba „%s“ obsahuje znak odřádkování"

#: src/nm-vpnc-service.c:231
#, c-format
msgid "property “%s” file path “%s” is not absolute or does not exist"
msgstr "cesta k souboru „%s“ u volby „%s“ není absolutní nebo neexistuje"

#: src/nm-vpnc-service.c:244
#, c-format
msgid "invalid integer property “%s” or out of range [%d -> %d]"
msgstr "celočíselná volba „%s“ není platná nebo je mimo rozsah [%d -> %d]"

#: src/nm-vpnc-service.c:254
#, c-format
msgid "invalid boolean property “%s” (not yes or no)"
msgstr "pravdivostní volba „%s“ není platná (není ano či ne)"

#: src/nm-vpnc-service.c:261
#, c-format
msgid "unhandled property “%s” type %d"
msgstr "neošetřená volba „%s“ typu %d"

#: src/nm-vpnc-service.c:278
msgid "No VPN configuration options."
msgstr "Žádné volby nastavení VPN."

#: src/nm-vpnc-service.c:304
msgid "No VPN secrets!"
msgstr "Žádná hesla VPN!"

#: src/nm-vpnc-service.c:649
msgid "Could not find vpnc binary."
msgstr "Nelze nalézt program vpnc."

#: src/nm-vpnc-service.c:767
#, c-format
msgid "Config option “%s” invalid or unknown."
msgstr "Volba nastavení „%s“ je neplatná nebo neznámá."

#: src/nm-vpnc-service.c:802
#, c-format
msgid "Config option “%s” not an integer."
msgstr "Volba nastavení „%s“ není číslo."

#: src/nm-vpnc-service.c:990
msgid "vpnc does not support interactive requests"
msgstr "vpnc nepodporuje interaktivní požadavky"

#: src/nm-vpnc-service.c:1014
msgid "Could not use new secrets as interactive mode is disabled."
msgstr "Nelze použít nové tajemství, protože je vypnutý interaktivní režim."

#: src/nm-vpnc-service.c:1023 src/nm-vpnc-service.c:1097
msgid ""
"Could not process the request because the VPN connection settings were "
"invalid."
msgstr "Nelze zpracovat požadavek protože nastavení VPN nejsou platná."

#: src/nm-vpnc-service.c:1031
msgid ""
"Could not process the request because no pending authentication is required."
msgstr ""
"Nelze zpracovat požadavek, protože není požadováno žádné čekající ověření."

#: src/nm-vpnc-service.c:1042
#, c-format
msgid ""
"Could not process the request because the requested info “%s” was not "
"provided."
msgstr ""
"Nelze zpracovat požadavek protože požadovaná informace „%s“ nebyla "
"poskytnuta."

#: src/nm-vpnc-service.c:1240
msgid "Don’t quit when VPN connection terminates"
msgstr "Neukončovat při uzavření spojení VPN"

#: src/nm-vpnc-service.c:1241
msgid "Enable verbose debug logging (may expose passwords)"
msgstr "Povolit podrobný ladicí výpis (může prozradit hesla)"

#: src/nm-vpnc-service.c:1242
msgid "D-Bus name to use for this instance"
msgstr "Název použitý pro tuto instanci v D-Bus"

#: src/nm-vpnc-service.c:1265
msgid ""
"nm-vpnc-service provides integrated Cisco Legacy IPsec VPN capability to "
"NetworkManager."
msgstr ""
"nm-vpnc-service umožňuje NetworkManageru poskytovat služby pro připojování k "
"Cisco IPsec VPN."

#: shared/nm-utils/nm-shared-utils.c:264
#, c-format
msgid "object class '%s' has no property named '%s'"
msgstr "objektová třída „%s“ nemá vlastnost nazvanou „%s“"

#: shared/nm-utils/nm-shared-utils.c:271
#, c-format
msgid "property '%s' of object class '%s' is not writable"
msgstr "vlastnost „%s“ objektové třídy „%s“ není zapisovatelná"

#: shared/nm-utils/nm-shared-utils.c:278
#, c-format
msgid ""
"construct property \"%s\" for object '%s' can't be set after construction"
msgstr ""
"vytvoření vlastnosti „%s“ objektu „%s“ nemůže být nastaveno po vytvoření"

#: shared/nm-utils/nm-shared-utils.c:286
#, c-format
msgid "'%s::%s' is not a valid property name; '%s' is not a GObject subtype"
msgstr "„%s::%s“ není platný název vlastnosti; „%s“ není podtyp GObject"

#: shared/nm-utils/nm-shared-utils.c:295
#, c-format
msgid "unable to set property '%s' of type '%s' from value of type '%s'"
msgstr "nelze nastavit vlastnost „%s“ typu „%s“ z hodnoty typu „%s“"

#: shared/nm-utils/nm-shared-utils.c:306
#, c-format
msgid ""
"value \"%s\" of type '%s' is invalid or out of range for property '%s' of "
"type '%s'"
msgstr "hodnota „%s“ typu „%s“ je mimo rozsah pro vlastnost „%s“ typu „%s“"

#: shared/nm-utils/nm-vpn-plugin-utils.c:69
#, c-format
msgid "unable to get editor plugin name: %s"
msgstr "nelze zjistit název zásuvného modulu editoru: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:103
#, c-format
msgid "missing plugin file \"%s\""
msgstr "chybějící soubor zásuvného modulu „%s“"

#: shared/nm-utils/nm-vpn-plugin-utils.c:109
#, c-format
msgid "cannot load editor plugin: %s"
msgstr "nelze nahrát editor zásuvného modulu: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:118
#, c-format
msgid "cannot load factory %s from plugin: %s"
msgstr "nelze nahrát generátor %s ze zásuvného modulu: %s"

#: shared/nm-utils/nm-vpn-plugin-utils.c:144
msgid "unknown error creating editor instance"
msgstr "neznámá chyba vytváření instance editoru"

#: properties/nm-vpnc-dialog.ui:19
msgid "General"
msgstr "Obecné"

#: properties/nm-vpnc-dialog.ui:36
msgid "_Gateway"
msgstr "_Brána"

#: properties/nm-vpnc-dialog.ui:49
msgid ""
"IP/hostname of IPsec gateway\n"
"config: IPSec gateway <gateway>"
msgstr ""
"IP/název počítače brány IPsec\n"
"config: IPSec gateway <brána>"

#: properties/nm-vpnc-dialog.ui:62
msgid "User na_me"
msgstr "Uživatelské j_méno"

#: properties/nm-vpnc-dialog.ui:75
msgid ""
"User name for the connection\n"
"config: Xauth username <user_name>"
msgstr ""
"Uživatelské jméno pro toto připojení\n"
"config: Xauth username <uživatelské_jméno>"

#: properties/nm-vpnc-dialog.ui:88
msgid "User _password"
msgstr "_Uživatelské heslo"

#: properties/nm-vpnc-dialog.ui:101
msgid ""
"User password for the connection\n"
"config: Xauth password <password>"
msgstr ""
"Heslo uživatele pro toto připojení\n"
"config: Xauth password <heslo>"

#: properties/nm-vpnc-dialog.ui:115
msgid "G_roup name"
msgstr "J_méno skupiny"

#: properties/nm-vpnc-dialog.ui:128
msgid ""
"Group name\n"
"config: IPSec ID <group_name>"
msgstr ""
"Název skupiny\n"
"config: IPSec ID <název_skupiny>"

#: properties/nm-vpnc-dialog.ui:141
msgid "Gro_up password"
msgstr "Heslo sk_upiny"

#: properties/nm-vpnc-dialog.ui:154
msgid ""
"Group password\n"
"config: IPSec secret <group_password>"
msgstr ""
"Heslo skupiny\n"
"config: IPSec secret <heslo_skupiny>"

#: properties/nm-vpnc-dialog.ui:165
msgid "Sho_w passwords"
msgstr "Zobrazo_vat hesla"

#: properties/nm-vpnc-dialog.ui:179
msgid "Use _hybrid authentication"
msgstr "Použít _hybridní ověření"

#: properties/nm-vpnc-dialog.ui:183
msgid ""
"Enable hybrid authentication, i.e. use certificate in addition to password.\n"
"config: IKE Authmode hybrid"
msgstr ""
"Povolit hybridní ověření, tzn. použití ceritikátu a hesla.\n"
"config: IKE Authmode hybrid"

#: properties/nm-vpnc-dialog.ui:237
msgid "Advance_d…"
msgstr "_Rozšířené…"

#: properties/nm-vpnc-dialog.ui:358
msgid "Advanced Options"
msgstr "Pokročilé volby"

#: properties/nm-vpnc-dialog.ui:375
msgid "Advanced Properties"
msgstr "Pokročilé vlastnosti"

#: properties/nm-vpnc-dialog.ui:383
msgid "_Cancel"
msgstr "_Zrušit"

#: properties/nm-vpnc-dialog.ui:391
msgid "_Apply"
msgstr "_Použít"

#: properties/nm-vpnc-dialog.ui:447
msgid "Identification"
msgstr "Identifikace"

#: properties/nm-vpnc-dialog.ui:464
msgid "_Domain"
msgstr "_Doména"

#: properties/nm-vpnc-dialog.ui:477
msgid ""
"(NT-)Domain name for authentication\n"
"config: Domain <domain>"
msgstr ""
"Název ověřovací domény (NT)\n"
"config: Domain <doména>"

#: properties/nm-vpnc-dialog.ui:490
msgid "_Vendor"
msgstr "_Výrobce"

#: properties/nm-vpnc-dialog.ui:503
#, fuzzy
#| msgid ""
#| "Vendor of your IPsec gateway\n"
#| "config: Vendor <cisco/netscreen>"
msgid ""
"Vendor of your IPsec gateway\n"
"config: Vendor <cisco/netscreen/fortigate>"
msgstr ""
"Výrobce vaší brány IPsec\n"
"config: Vendor <cisco|netscreen>"

#: properties/nm-vpnc-dialog.ui:523
msgid "Ve_rsion"
msgstr "Ve_rze"

#: properties/nm-vpnc-dialog.ui:535
msgid ""
"Application version to report. \n"
"config: Application version <ASCII string>"
msgstr ""
"Oznamovaná verze aplikace.\n"
"config: Application version <řetězec ASCII>"

#: properties/nm-vpnc-dialog.ui:550
msgid "Transport and Security"
msgstr "Přenos a bezpečnost"

#: properties/nm-vpnc-dialog.ui:566
msgid "Tunnel _interface name"
msgstr "Název rozhraní _tunelu"

#: properties/nm-vpnc-dialog.ui:578
msgid ""
"Tunnel interface name to use\n"
"config: Interface name <name>"
msgstr ""
"Název rozhraní tunelu, který bude použit\n"
"config: Interface name <název>"

#: properties/nm-vpnc-dialog.ui:592 properties/nm-vpnc-dialog.ui:604
msgid "Interface MTU"
msgstr ""

#: properties/nm-vpnc-dialog.ui:616
msgid "_Encryption method"
msgstr "M_etoda šifrování"

#: properties/nm-vpnc-dialog.ui:628
msgid ""
"Encryption method\n"
"config: nothing for Secure, “Enable Single DES” for Weak, “Enable no "
"encryption” for None"
msgstr ""
"Metoda šifrování\n"
"config: nic pro bezpečné, „Enable Single DES“ pro slabé, „Enable no "
"encryption“ pro žádné"

#: properties/nm-vpnc-dialog.ui:648
msgid "_NAT traversal"
msgstr "Překonávání _NAT"

#: properties/nm-vpnc-dialog.ui:660
msgid ""
"NAT traversal method to use\n"
"config: NAT Traversal Mode <natt/none/force-natt/cisco-udp>"
msgstr ""
"Použitá metoda překonávání NAT\n"
"config: NAT Traversal Mode <natt|none|force-natt|cisco-udp>"

#: properties/nm-vpnc-dialog.ui:680
msgid "_IKE DH Group"
msgstr "Skupina IKE DH"

#: properties/nm-vpnc-dialog.ui:692
#, fuzzy
#| msgid ""
#| "Name of the IKE DH group\n"
#| "config: IKE DH Group <dh1/dh2/dh5>"
msgid ""
"Name of the IKE DH group\n"
"config: IKE DH Group <dh1/dh2/dh5/dh14/dh15/dh16/dh17/dh18>"
msgstr ""
"Název skupiny IKE DH\n"
"config: IKE DH Group <dh1|dh2|dh5>"

#: properties/nm-vpnc-dialog.ui:712
msgid "Perfect _Forward Secrecy"
msgstr "Perfect _Forward Secrecy"

#: properties/nm-vpnc-dialog.ui:725
#, fuzzy
#| msgid ""
#| "Diffie-Hellman group to use for PFS\n"
#| "config: Perfect Forward Secrecy <nopfs/dh1/dh2/dh5/server>"
msgid ""
"Diffie-Hellman group to use for PFS\n"
"config: Perfect Forward Secrecy <nopfs/dh1/dh2/dh5/dh14/dh15/dh16/dh17/dh18/"
"server>"
msgstr ""
"Skupina Diffie-Hellman použitá pro PFS\n"
"config: Perfect Forward Secrecy <nopfs|dh1|dh2|dh5|server>"

#: properties/nm-vpnc-dialog.ui:745
msgid "_Local port"
msgstr "Mís_tní port"

#: properties/nm-vpnc-dialog.ui:757
msgid ""
"Local ISAKMP port to use (0 means random port; 500 is vpnc’s default)\n"
"config: Local Port <0-65535>"
msgstr ""
"Místní port ISAKMP, který bude použit (0 znamená náhodně vybraný port; 500 "
"je výchozí pro vpnc)\n"
"config: Local Port <0-65535>"

#: properties/nm-vpnc-dialog.ui:760
msgid "0"
msgstr "0"

#: properties/nm-vpnc-dialog.ui:773
msgid "Disable Dead _Peer Detection"
msgstr "Zakázat Dead _Peer Detection"

#: properties/nm-vpnc-dialog.ui:777
msgid ""
"Disable sending DPD packets (sets timeout to 0)\n"
"config: DPD idle timeout (our side) 0"
msgstr ""
"Zablokovat posílání rámců DPD (nastaví časový limit na 0)\n"
"config: DPD idle timeout (our side) 0"

#: properties/nm-vpnc-dialog.ui:790
msgid "Enable weak (insecure) authentication"
msgstr ""

#: properties/nm-vpnc-dialog.ui:794
msgid ""
"Enable use of known insecure algorithms (such as MD5) for password hashing"
msgstr ""

#: properties/nm-vpnc-dialog.ui:806
msgid "Enable weak (insecure) encryption"
msgstr ""

#: properties/nm-vpnc-dialog.ui:810
msgid ""
"Enable use of known insecure algorithms (such as DES and 3DES) for encryption"
msgstr ""
